
export default defineNuxtConfig({
    css: ['~/assets/css/main.css'],
    postcss: {
        plugins: {
            tailwindcss: {},
            autoprefixer: {},
        },
    },
    modules: [ '@pinia/nuxt'],
    runtimeConfig: {
        public: {
            baseURL: process.env.URL_API
        }
    }
})
