import {defineStore} from 'pinia';
import {useMyFetch} from "~/composables/useMyFetch";

interface Subjects {
    listSubject: any[];
}

export const subjectStore = defineStore('subjects', {
    state: (): Subjects => {
        return {
            listSubject: [],
        };
    },
    actions: {
        async getListSubject() {
            const data = await useMyFetch(`/subjects`)
            if (data && data.status){
                this.listSubject = data.data
                return data.data
            }
        },
    },
});