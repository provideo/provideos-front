import { UseFetchOptions } from '#app'
import { NitroFetchRequest } from 'nitropack'
import { KeyOfRes } from 'nuxt/dist/app/composables/asyncData'
import JWTService from "~/common/jwt.service";

export function useMyFetch<T>(
    request: NitroFetchRequest,
    opts?:
        | UseFetchOptions<
        T extends void ? unknown : T,
        (res: T extends void ? unknown : T) => T extends void ? unknown : T,
        KeyOfRes<
            (res: T extends void ? unknown : T) => T extends void ? unknown : T
            >
        >
        | undefined
) {
    // @ts-ignore
    const config = useRuntimeConfig()
    const token = JWTService.getToken()
    let authorization = ''
    if(token) {
        authorization = `Bearer ${token}`
    }
    return $fetch<T>(request, {
        baseURL: config.public.baseURL,
        initialCache: false,
        headers: {
            Authorization: authorization,
        },
        ...opts,
    })
}